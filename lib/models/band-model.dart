/* class Band {
  String id;
  String name;
  int votes;
  Band({
    this.id,
    this.name,
    this.votes,
  });
  factory Band.fromMap(Map<String, dynamic> obj) => Band(
    id: obj['id'],
    name: obj['name'],
    votes: obj['votes'],
  );
}
 */


import 'dart:convert';

BandsModel bandsModelFromJson(String str) => BandsModel.fromJson(json.decode(str));

String bandsModelToJson(BandsModel data) => json.encode(data.toJson());

class BandsModel {
    BandsModel({
        this.bands,
    });

    List<Band> bands;

    factory BandsModel.fromJson(Map<String, dynamic> json) => BandsModel(
        bands: List<Band>.from(json["bands"].map((x) => Band.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "bands": List<dynamic>.from(bands.map((x) => x.toJson())),
    };
}

class Band {
    Band({
        this.id,
        this.name,
        this.votes,
    });

    String id;
    String name;
    int votes;

    factory Band.fromJson(Map<String, dynamic> json) => Band(
        id: json["id"],
        name: json["name"],
        votes: json["votes"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "votes": votes,
    };
}
