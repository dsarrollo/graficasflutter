import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'pages/home-page.dart';
import 'pages/status-page.dart';
import 'services/sockect-services.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SocketService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Graficos',
        initialRoute: HomePage.routerName,
        routes: {
          HomePage.routerName: (BuildContext c) => HomePage(),
          StatusPage.routerName: (BuildContext c) => StatusPage(),
        },
      ),
    );
  }
}
