import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:graficas/models/band-model.dart';
// import 'package:socket_io/socket_io.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

enum ServerStatus {
  Offline,
  Online,
  Connecting,
}

class SocketService with ChangeNotifier {
  List<Band> _bans = [];

  ServerStatus _serverStatus = ServerStatus.Connecting;
  IO.Socket _socket;

  ServerStatus get serverStatus => this._serverStatus;
  IO.Socket get socket => this._socket;
  List<Band> get bands => this._bans;

  SocketService() {
    this._initConfig();
  }

  void _initConfig() {
    this._socket = IO.io('https://flutter-graph.herokuapp.com', {
      'transports': ['websocket'],
      'autoConnect': true,
    });

    this._socket.on('connect', (_) {
      this._serverStatus = ServerStatus.Online;
      notifyListeners();
    });

    this._socket.on('getBands', (data) {
      final bans = bandsModelFromJson(data.toString());
      this._bans = bans.bands;
      notifyListeners();
    });

    this._socket.on('deleteBand', (data) {
      this._bans.removeWhere((element) => element.id == data['id']);
      notifyListeners();
    });

    this._socket.on('voteBand', (data) {
      this
          ._bans[this._bans.indexWhere((element) => element.id == data['id'])]
          .votes++;
      notifyListeners();
    });

    this._socket.on('addBand', (data) {
      this._bans.add(Band.fromJson(json.decode(data)));
      notifyListeners();
    });

    this._socket.on('disconnect', (_) {
      print('Desconectando del seridor');
      this._serverStatus = ServerStatus.Offline;
      notifyListeners();
    });
  }
}
