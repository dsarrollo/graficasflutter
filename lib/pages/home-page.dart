import 'package:flutter/material.dart';
import 'package:graficas/services/sockect-services.dart';
import 'package:provider/provider.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:graficas/models/band-model.dart';

class HomePage extends StatefulWidget {
  static String routerName = 'home';
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final socketService = Provider.of<SocketService>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Material App Bar',
          style: TextStyle(
            color: Colors.black87,
          ),
        ),
        backgroundColor: Colors.white,
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: (socketService.serverStatus == ServerStatus.Online)
                ? Icon(Icons.check_circle, color: Colors.green)
                : Icon(Icons.offline_bolt, color: Colors.red),
          )
        ],
      ),
      body: Column(children: [
        _graph(socketService),
        Expanded(
          child: ListView.builder(
            itemCount: socketService.bands.length,
            itemBuilder: (c, index) =>
                _listBand(socketService.bands[index], socketService),
          ),
        ),
      ]),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _addNewBand(socketService),
      ),
    );
  }

  Widget _listBand(Band item, SocketService socketService) {
    return Dismissible(
      key: Key(item.id),
      direction: DismissDirection.startToEnd,
      onDismissed: (_) =>
          socketService.socket.emit('deleteBand', {'id': item.id}),
      background: Container(
        padding: EdgeInsets.only(left: 10.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Eliminar band',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
        color: Colors.green[400],
      ),
      child: ListTile(
        leading: CircleAvatar(
          child: Text(
            item.name.substring(0, 2),
            style: TextStyle(
              color: Colors.black54,
            ),
          ),
          backgroundColor: Colors.green[100],
        ),
        title: Text(item.name),
        trailing: Text(
          '${item.votes}',
          style: TextStyle(fontSize: 20),
        ),
        onTap: () => socketService.socket.emit('voteBand', {'id': item.id}),
      ),
    );
  }

  void _addNewBand(SocketService socketService) {
    final textController = TextEditingController();
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('New band name'),
          content: TextField(
            controller: textController,
          ),
          actions: [
            MaterialButton(
              child: Text('Guardar'),
              textColor: Colors.green,
              onPressed: () {
                if (textController.text.length > 0) {
                  socketService.socket
                      .emit('addBand', {'name': textController.text});
                }
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  Widget _graph(SocketService socketService) {
    Map<String, double> dataMap = new Map();
    socketService.bands.forEach((element) {
      dataMap.putIfAbsent(element.name, () => element.votes.toDouble());
    });
    final List<Color> colorList = [
      Colors.blue[100],
      Colors.blue[500],
      Colors.green[100],
      Colors.green[500],
      Colors.yellow[100],
      Colors.yellow[500],
      Colors.purple[100],
      Colors.purple[500],
      Colors.pink[100],
      Colors.pink[500],
    ];
    return Container(
      width: double.infinity,
      height: 200.0,
      child: (dataMap.length > 0)
          ? PieChart(
              dataMap: dataMap,
              animationDuration: Duration(milliseconds: 800),
              chartLegendSpacing: 32,
              chartRadius: MediaQuery.of(context).size.width / 3.2,
              colorList: colorList,
              initialAngleInDegree: 0,
              chartType: ChartType.ring,
              ringStrokeWidth: 32,
              centerText: "BTC",
              legendOptions: LegendOptions(
                showLegendsInRow: false,
                legendPosition: LegendPosition.right,
                showLegends: true,
                // legendShape: _BoxShape.circle,
                legendTextStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              chartValuesOptions: ChartValuesOptions(
                showChartValueBackground: false,
                showChartValues: true,
                showChartValuesInPercentage: false,
                showChartValuesOutside: false,
              ),
            )
          : Placeholder(),
    );
  }
}
