import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graficas/services/sockect-services.dart';

class StatusPage extends StatelessWidget {
  const StatusPage({Key key}) : super(key: key);
  static String routerName = 'status';
  @override
  Widget build(BuildContext context) {
    final sokectService = Provider.of<SocketService>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Status',
          style: TextStyle(
            color: Colors.black87,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: Center(
        child: Text('Status socket ${sokectService.serverStatus}'),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.message_outlined),
        onPressed: (){
          sokectService.socket.emit('newMessage', {'name': 'Eduardo'});
        },
      ),
    );
  }
}
